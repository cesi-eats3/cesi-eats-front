#!/bin/bash

# Vérifie si le répertoire /var/www existe, sinon le crée
if [ ! -d "/var/www" ]; then
    echo "Le répertoire /var/www n'existe pas. Création..."
    sudo mkdir -p /var/www
    sudo chown $USER:$USER /var/www
fi

# Vérifie si le répertoire /var/www/nginx existe, sinon le crée
if [ ! -d "/var/www/nginx" ]; then
    echo "Le répertoire /var/app/nginx n'existe pas. Création..."
    sudo mkdir -p /var/www/nginx
    sudo chown $USER:$USER /var/www/nginx
fi

# Répertoire du projet
project_dir="/var/www/nginx/cesi-eats-front"

# Vérifie si le répertoire existe
if [ ! -d "$project_dir" ]; then
    echo "Le répertoire $project_dir n'existe pas. Clonage du dépôt..."
    # Crée le répertoire et clone le dépôt
    sudo mkdir -p "$project_dir"
    sudo chown $USER:$USER "$project_dir"
    git clone https://gitlab.com/cesi-eats3/cesi-eats-front.git "$project_dir"
else
    echo "Le répertoire $project_dir existe. Mise à jour du dépôt..."
    # Accéder au répertoire et effectuer un pull
    cd "$project_dir"
    git pull
fi
